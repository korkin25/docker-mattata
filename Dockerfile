FROM ubuntu:bionic

ENV DEBIAN_FRONTEND noninteractive

RUN echo deb http://ru.archive.ubuntu.com/ubuntu/ bionic main restricted universe multiverse > /etc/apt/sources.list
RUN echo deb http://ru.archive.ubuntu.com/ubuntu/ bionic-updates main restricted universe multiverse >> /etc/apt/sources.list
RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y apt-utils

RUN apt-get install -y locales
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

RUN ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime

ENV LANG en_US.UTF-8 

#seems to be need for
RUN apt-get install -y libssl-dev

COPY mattata mattata/

WORKDIR mattata
RUN cat install.sh | sed 's/sudo //g' | bash -x

ENTRYPOINT [ "/mattata/launch.sh" ]

